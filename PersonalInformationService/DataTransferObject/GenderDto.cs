﻿using System.Runtime.Serialization;

namespace PersonalInformationService.DataTransferObject 
{
    [DataContract(Name ="Gender", Namespace = "PersonalInformationService.Types")]
    public enum GenderDto
    {
        [EnumMember]
        Female,

        [EnumMember]
        Male,

    }
}
