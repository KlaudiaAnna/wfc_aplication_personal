﻿using System.Runtime.Serialization;

namespace PersonalInformationService.DataTransferObject
{
    [DataContract(Name = "Person", Namespace ="PersonalInformationService.Types")]
   public class PersonDto
    {

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Surname { get; set; }
        public GenderDto Gender { get;  set; }
    }
}
