﻿using System.Collections.Generic;
using PersonalInformationService.Messages;
using PersonalInformationService.DataTransferObject;
using PersonalInformationService.ServiceContracts;

namespace PersonalInformationService.ServiceImplementations
{
   public class SimplePersonalInformationService : IPersonalInformationService
    {
        public PersonalInformationResponse GetPersonalInformation
                (PersonalInformationRequest request)
        {
            PersonalInformationResponse response = new PersonalInformationResponse();
            response.Persons = new List<PersonDto>();
            response.Persons.Add(new PersonDto()
            {
                Id = request.PersonId,
                Name = "Maciej",
                Surname = "Kowalski",
                Gender = GenderDto.Male
            });
            return response;
        }
    }
}
