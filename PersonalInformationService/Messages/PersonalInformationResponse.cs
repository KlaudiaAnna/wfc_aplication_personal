﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PersonalInformationService.DataTransferObject;

namespace PersonalInformationService.Messages
{
    [DataContract]
   public class PersonalInformationResponse
    {
        [DataMember]
        public List<PersonDto> Persons { get; set; }

    }
}
