﻿using PersonalInformationService.Messages;
using System.ServiceModel;

namespace PersonalInformationService.ServiceContracts
{
    [ServiceContract]
    public interface IPersonalInformationService
    {
        [OperationContract]
        PersonalInformationResponse GetPersonalInformation
            (PersonalInformationRequest request);
    }






}
