using System;
using System;
using System.ServiceModel;
using PersonalInformationService.ServiceImplementations;

namespace PersonalInformationHosts.SelfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            RunHost();
        }
        private static void RunHost()
        {
            using (ServiceHost host =
            new ServiceHost(typeof(SimplePersonalInformationService)))
            {
                Console.WriteLine("Personal Information Service host starting");
                host.Open();
                Console.WriteLine("Press [ENTER] to stop service...");
                Console.ReadLine();
            }
        }
    }
}
